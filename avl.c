#include "avl.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct nodo_fila {
    struct nodo *elem;
    struct nodo_fila *prox;
};

struct fila {
    struct nodo_fila *ini;
    struct nodo_fila *fim;
    int tamanho; /* numero de elementos na fila */
};

//substitua por seus dados
struct aluno* getAluno1(){
    struct aluno* retorno = malloc(sizeof(struct aluno));
    if(!retorno)
        exit(1);
    retorno->nome = malloc(sizeof("João Marcelo Sales Caboclo Ribeiro"));//sizeof conta o \0
	if(!retorno->nome)
		exit(1);
    retorno->nomeDinf = malloc(sizeof("jmscr22"));
    if(!retorno->nomeDinf)
        exit(1);
    
	strcpy(retorno->nome, "João Marcelo Sales Caboclo Ribeiro");
    strcpy(retorno->nomeDinf, "jmscr22");
    retorno->grr = 20221227;

	return retorno;
}

struct aluno* getAluno2(){
    return NULL;
}

void imprimirDadosAlunos(){
    struct aluno* aluno = getAluno1();
    
    printf("Trabalho de %s\n", aluno->nome);
    printf("Login Dinf %s\n", aluno->nomeDinf);
    printf("GRR %u\n\n", aluno->grr);

    free(aluno->nome);
    free(aluno->nomeDinf);
    free(aluno);

    aluno = getAluno2();
    if(!aluno)
        return;
    
    printf("...E... \n\n");
    printf("Trabalho de %s\n", aluno->nome);
    printf("Login Dinf %s\n", aluno->nomeDinf);
    printf("GRR %u\n\n", aluno->grr);

    free(aluno->nome);
    free(aluno->nomeDinf);
    free(aluno);
}

struct nodo* cria_nodo(int chave)
{
    struct nodo *no = malloc(sizeof(struct nodo));

    if (!no){
        perror("Erro no malloc\n");
        return NULL;
    }

    no->chave = chave;
    /*todo novo nó é uma folha, portanto alt = 0*/
    no->altura = 0;
    no->filho_direito = NULL;
    no->filho_esquerdo = NULL;
    no->pai = NULL;

}

/*Retorna a maior altura entre as subarvores de um nó*/
int maior(int altura_a, int altura_b)
{
    if (altura_a >= altura_b){
        return altura_a;
    }
    else{
        return altura_b;
    }
}

/*Retorna a aultura de um nó, se nao existir retorna -1*/
int altura_nodo(struct nodo *no)
{
    if(no == NULL){
        return -1;
    }
    else{
        return no->altura;
    }
}

/*Retorna o fator de balanceamento de um nó a partir da equaçao de fb.
Como a arvore estara sempre sendo balanceada o fb pode ser um short*/
short fator_de_balanceamento(struct nodo* no)
{
    if(!no){
        return 0;
    }

    short fb;
    fb = altura_nodo(no->filho_esquerdo) - altura_nodo(no->filho_direito);
    return fb;
}

void libera_arvore(struct nodo *raiz)
{
    if (!raiz){
        return;
    }
    libera_arvore(raiz->filho_esquerdo); 
    libera_arvore(raiz->filho_direito);
    free(raiz);
}

int altura_atual(struct nodo *no)
{
    int altura_e = altura_nodo(no->filho_esquerdo);
    int altura_d = altura_nodo(no->filho_direito);
    int altura = maior(altura_e, altura_d) + 1;
    return altura;
}

/*Rotacioana a esquerda*/
void rotacao_esquerda(struct nodo **raiz, struct nodo *no_desbalanceado)
{
    /*Salva o nó que sera reposicionado*/
    struct nodo *ponteiro_filho = no_desbalanceado->filho_direito;;
    no_desbalanceado->filho_direito = ponteiro_filho->filho_esquerdo;

    /*Posiciona o possivel filho esquerdo da nova sub raiz*/
    if (ponteiro_filho->filho_esquerdo != NULL){
        ponteiro_filho->filho_esquerdo->pai = no_desbalanceado;
    }

    /*troca o pai do ponteiro filho e do nó desbalanceado*/
    ponteiro_filho->pai = no_desbalanceado->pai;

    /*Caso o no_desbalanceado seja a antiga raiz*/
    if (no_desbalanceado->pai == NULL){
        (*raiz) = ponteiro_filho;
    }
    /*Caso o antigo no desbalanceado tenha sido filho esquerdo de outro nó*/
    else if (no_desbalanceado == no_desbalanceado->pai->filho_esquerdo){
        no_desbalanceado->pai->filho_esquerdo = ponteiro_filho;
    }
    /*Caso o antigo no desbalanceado tenha sido filho direito de outro nó*/
    else{
        no_desbalanceado->pai->filho_direito = ponteiro_filho;
    }

    ponteiro_filho->filho_esquerdo = no_desbalanceado;
    no_desbalanceado->pai = ponteiro_filho;

    /*Atualiza as alturas*/
    no_desbalanceado->altura = altura_atual(no_desbalanceado);
    ponteiro_filho->altura = altura_atual(ponteiro_filho);
}

/*Rotaciona a direita*/
void rotacao_direita(struct nodo **raiz, struct nodo *no_desbalanceado)
{   
    /* Salva o nó que será reposicionado */
    struct nodo *ponteiro_filho = no_desbalanceado->filho_esquerdo;
    no_desbalanceado->filho_esquerdo = ponteiro_filho->filho_direito;

    /* Posiciona o possível filho direito da nova sub raiz */
    if (ponteiro_filho->filho_direito != NULL) {
        ponteiro_filho->filho_direito->pai = no_desbalanceado;
    }

    /* Troca o pai do ponteiro filho e do nó desbalanceado */
    ponteiro_filho->pai = no_desbalanceado->pai;

    /* Caso o no_desbalanceado seja a antiga raiz */
    if (no_desbalanceado->pai == NULL) {
        (*raiz) = ponteiro_filho;
    }
    /* Caso o antigo no desbalanceado tenha sido filho direito de outro nó */
    else if (no_desbalanceado == no_desbalanceado->pai->filho_direito) {
        no_desbalanceado->pai->filho_direito = ponteiro_filho;
    }
    /* Caso o antigo no desbalanceado tenha sido filho esquerdo de outro nó */
    else {
        no_desbalanceado->pai->filho_esquerdo = ponteiro_filho;
    }

    ponteiro_filho->filho_direito = no_desbalanceado;
    no_desbalanceado->pai = ponteiro_filho;

    /* Atualiza as alturas */
    no_desbalanceado->altura = altura_atual(no_desbalanceado);
    ponteiro_filho->altura = altura_atual(ponteiro_filho);
}


/*Rotaciona direita-esquerda*/
void rotacao_direita_esquerda(struct nodo **raiz, struct nodo *no_desbalanceado)
{

    /*Rotaciona primeiro a direita o filho direito e depois a sub arvore a esquerda*/ 
    rotacao_direita(raiz, no_desbalanceado->filho_direito);
    rotacao_esquerda(raiz, no_desbalanceado);

}

void rotacao_esquerda_direita(struct nodo **raiz, struct nodo *no_desbalanceado)
{

    /*Rotaciona primeiro a esquerda o filho esquerdo e depois a sub arvore a direita*/
    rotacao_esquerda(raiz, no_desbalanceado->filho_esquerdo);
    rotacao_direita(raiz, no_desbalanceado);

}

void balanceamento(struct nodo **raiz)
{
    /*Calcula o fator de balanceamento da raiz e seus filhos*/
    short fb_raiz = fator_de_balanceamento((*raiz));
    short fb_direito = fator_de_balanceamento((*raiz)->filho_direito);
    short fb_esquerdo = fator_de_balanceamento((*raiz)->filho_esquerdo);

    /*Rotacao equerda quando pende a direita e o filho tambem*/
    if(fb_raiz < -1 && fb_direito <= 0){
        rotacao_esquerda(raiz, (*raiz));
        return;
    }

    /*Rotacao a direita quando pende a esquerda e o filho tambem*/
    else if(fb_raiz > 1 && fb_esquerdo >= 0){
        rotacao_direita(raiz, (*raiz));
        return;
    }

    /*Rotacao esquerda direita quando pende a esquerda mas o filho a direita*/
    else if(fb_raiz > 1 && fb_esquerdo < 0){
        rotacao_esquerda_direita(raiz, (*raiz));
        return;
    }

    /*Rotacao direita esquerda quando pende a direita mas o filho a esquerda*/
    else if(fb_raiz < -1 && fb_direito > 0){
        rotacao_direita_esquerda(raiz, (*raiz));
        return;
    }

}

void insere(struct nodo** raiz, struct nodo* novo_no, int* n_tem)
{
    /* Arvore vazia ou chegou em uma folha */
    if (*raiz == NULL) {

        *raiz = novo_no;
        *n_tem = 1;
        return;
    } 

    /*Posiciona o pai do novo_nó*/
    novo_no->pai = (*raiz);
    
    if (novo_no->chave < (*raiz)->chave){
        insere(&(*raiz)->filho_esquerdo, novo_no, n_tem);
    }
    else if (novo_no->chave > (*raiz)->chave){
        insere(&(*raiz)->filho_direito, novo_no, n_tem);
    }
    else{
        *n_tem = 0;
        return;
    }

    /* Corrige a altura e balanceamento de todos os nos entre a raiz e o nodo inserido */
    (*raiz)->altura = altura_atual(*raiz);
    balanceamento(raiz);

}

struct nodo* inserir(struct nodo** raiz, int chave)
{
    struct nodo *novo_no = cria_nodo(chave);
    int n_tem = 0;

    insere(raiz, novo_no, &n_tem);

    if (!n_tem){
        free(novo_no);
        return NULL;
    }
    return novo_no;
}

/*Maior filho da subarvore esquerda*/
struct nodo *antecessor(struct nodo* nodo)
{
    struct nodo *no = nodo->filho_esquerdo;

    while (no->filho_direito != NULL){
        no = no->filho_direito;
    }
    return no;
}

/*Troca dois nos de posicao e ajusta os ponteiros*/
void transplanta(struct nodo** raiz, struct nodo** no_1, struct nodo** no_2)
{
    /*Caso o no_1 seja a raiz*/
    if ((*no_1)->pai == NULL){
        (*raiz) = (*no_2);
    }

    /*Caso o no_1 seja filho esquerdo do seu pai*/
    else if ((*no_1) == (*no_1)->pai->filho_esquerdo){
        (*no_1)->pai->filho_esquerdo = (*no_2);
    }
    /*Caso o no_1 seja filho direito do seu pai*/
    else if ((*no_1) == (*no_1)->pai->filho_direito){
        (*no_1)->pai->filho_direito = (*no_2);
    }

    if ((*no_2) != NULL){
        (*no_2)->pai = (*no_1)->pai;
    }
}

void apagar(struct nodo** raiz, struct nodo* no_apagado)
{
    /*Caso o nó apagado nao tenha um filho esquerdo*/
    if (no_apagado->filho_esquerdo == NULL){
        transplanta(raiz, &no_apagado, &no_apagado->filho_direito);
    }

    /*Caso o nó apagado nao tenha um filho direito*/
    else if (no_apagado->filho_direito == NULL){
        transplanta(raiz, &no_apagado, &no_apagado->filho_esquerdo);
    }

    else{
        struct nodo *anterior = antecessor(no_apagado);
        struct nodo *pai_antecessor = anterior->pai;

        if (anterior->pai != no_apagado){
            transplanta(raiz, &anterior, &anterior->filho_esquerdo);
            anterior->filho_esquerdo = no_apagado->filho_esquerdo;
            anterior->filho_esquerdo->pai = anterior;
        }

        transplanta(raiz, &no_apagado, &anterior);
        anterior->filho_direito = no_apagado->filho_direito;

        if (anterior->filho_direito != NULL){
            anterior->filho_direito->pai = anterior;
        }

        while ((pai_antecessor != NULL) && (pai_antecessor != no_apagado)){
            pai_antecessor->altura = altura_atual(pai_antecessor);
            balanceamento(&pai_antecessor);
            pai_antecessor = pai_antecessor->pai;
        }
    }
    free(no_apagado);
}

void deleta(struct nodo** raiz, int chave, int *n_tem)
{
    /*Se a chave nao foi encontrada*/
    if ((*raiz) == NULL){
        *n_tem = 1;
        return;
    }
    /*Algoritimo de busca*/
    if (chave < (*raiz)->chave){
        deleta(&(*raiz)->filho_esquerdo, chave, n_tem);
    }
    else if (chave > (*raiz)->chave){
        deleta(&(*raiz)->filho_direito, chave, n_tem);
    }
    /*Chama a funcao que vai remover e reconectar os nós*/
    else{
        *n_tem = 0;
        apagar(raiz, (*raiz));
    }
    /*Balanceia e ajusta a altura dos nos entre a raiz e o nó removido*/
    if ((*raiz) != NULL){
        (*raiz)->altura = altura_atual(*raiz);
        balanceamento(raiz);
    }

}

int excluir(struct nodo** raiz, int chave)
{
	int n_tem = 0;

    deleta(raiz, chave, &n_tem);

    if (n_tem){
        return 0;
    }
    return 1;
}

struct nodo* buscar(struct nodo* nodo, int chave)
{
	if (nodo == NULL || nodo->chave == chave){
        return nodo;
    }

    if (chave < nodo->chave){
        return buscar(nodo->filho_esquerdo, chave);
    }

    return buscar(nodo->filho_direito, chave);
}

/*Chave (chave pai)(balanceamento)(altura)*/
void imprimirDebug(struct nodo* raiz, int nivel)
{
    int i;
    if(raiz){
        imprimirDebug(raiz->filho_direito, nivel + 1);
        printf("\n\n");

        for (i = 0; i < nivel; i++)
            printf("\t");

        if (!raiz->pai)
            printf("%d (raiz)(%hi)(%d)", raiz->chave, fator_de_balanceamento(raiz), altura_nodo(raiz));
        else
            printf("%d (%d)(%hi)(%d)", raiz->chave, raiz->pai->chave, fator_de_balanceamento(raiz), altura_nodo(raiz));
        imprimirDebug(raiz->filho_esquerdo, nivel + 1);
    }
}

void imprimirEmOrdem(struct nodo* raiz)
{ 
	if (raiz != NULL) {

        /*Imprime os nós da subárvore esquerda*/
        imprimirEmOrdem(raiz->filho_esquerdo);

        /*Imprime a chave do nó atual*/
        printf("%d ", raiz->chave);

        /*Imprime os nós da subárvore direita*/
        imprimirEmOrdem(raiz->filho_direito);
    }
}

struct fila *cria_fila ()
{
    struct fila *f = malloc(sizeof(struct fila));

    if(!f)
        return NULL;
    
    f->ini = NULL;    
    f->fim = NULL;
    f->tamanho = 0;

    return f;
}

struct fila *destroi_fila (struct fila *f)
{	
	if(!f)
		return NULL;
	
    struct nodo_fila *aux;
    
    while (f->ini != NULL){
        aux = f->ini;
        f->ini = f->ini->prox;
        free(aux);
    } 
    
    free(f);
	
	return NULL;
}

int vazia_fila (struct fila *f)
{
    if(!f)
        return 1;
    
    return !f->ini;
}


int insere_fila (struct fila *f, struct nodo *elemento)
{
    struct nodo_fila *novo = malloc(sizeof(struct nodo_fila));
    if(!novo || !f)
        return 0;

    novo->elem = elemento;
    novo->prox = NULL;

    if(vazia_fila(f)){
        f->ini = novo;
    }

    else{
        f->fim->prox = novo;
    }
    f->fim = novo;
    f->tamanho++;
        
    return 1;
}

struct nodo *retira_fila (struct fila *f)
{
    struct nodo_fila *remover = NULL;

    if(!f || !f->ini)
        return 0;

    remover = f->ini;
    f->ini = remover->prox;
    f->tamanho--;

    struct nodo *valor = remover->elem;
    free(remover);

    return valor;

}

void imprimirEmLargura(struct nodo* raiz)
{
	if (raiz == NULL) {
        return;
    }

    int nivel = 0;
    struct nodo* anterior;
    struct fila* fila = cria_fila();
    insere_fila(fila, raiz);

    while (!vazia_fila(fila)){
        struct nodo* noAtual = retira_fila(fila);
        if (nivel == 0){
            printf("[%d] ", nivel);
            anterior = noAtual;
            nivel++;
        }

        if (noAtual->chave < anterior->chave || noAtual->pai == anterior){
            printf("\n[%d] ", nivel);
            nivel++;
        }

        printf("%d(%d) ", noAtual->chave, fator_de_balanceamento(noAtual));

        if (noAtual->filho_esquerdo != NULL) {
            insere_fila(fila, noAtual->filho_esquerdo);
        }
        if (noAtual->filho_direito != NULL) {
            insere_fila(fila, noAtual->filho_direito);
        }
        
        anterior = noAtual;
    }

    destroi_fila(fila);
}
