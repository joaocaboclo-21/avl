#include <stdio.h>
#include <stdlib.h>

#include "avl.h"

int main() {
    char operacao;
    int chave;

    imprimirDadosAlunos();

    struct nodo *aponta_raiz = NULL;
    scanf("%c", &operacao);
    while (operacao != 'f') {
        switch (operacao) {
            case 'i':
                scanf("%d", &chave);

                if (!inserir(&aponta_raiz, chave)) {
                    printf("Falha ao inserir\n");
                }
                break;

            case 'r':
                scanf("%d", &chave);

                if (!excluir(&aponta_raiz, chave)) {
                    printf("Falha ao remover %d\n", chave);
                }
                break;

            case 'b':
                scanf("%d", &chave);

                if (buscar(aponta_raiz, chave)) {
                    printf("Encontrado %d\n", chave);
                } else {
                    printf("Nao encontrado %d\n", chave);
                }
                break;

            /*case 'd':
                printf("Imprimir em forma de Debug\n");
                imprimirDebug(aponta_raiz, 1);
                printf("\n");
                break;*/

            case 'e':
                imprimirEmOrdem(aponta_raiz);
                printf("\n");
                break;

            case 'l':
                imprimirEmLargura(aponta_raiz);
                printf("\n");
                break;
			
			default:
				break;
        }

        scanf(" %c", &operacao);
    }
    libera_arvore(aponta_raiz);
    return 0;
}
