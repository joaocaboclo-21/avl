nome: João Marcelo Sales Caboclo Ribeiro
grr: 20221227

1. main:
    A função main tem como propósito selecionar qual operação será realizada fazendo isso através da utilização da função switch. Se a operação digitada não existir
nenhuma função será chamada e uma nova opção de operação deve ser digitada.

2. balanceamento:
    A função “balanceamento” consiste de outras funções:
	2.1. fator_de_balanceamento:
        Essa função tem como único propósito retornar o valor de balanceamento, ou seja, a altura do filho esquerdo do nó menos a altura do filho direito.
    2.2. Rotações únicas e duplas:
        As funções de rotação únicas analisam os possíveis casos, como por exemplo, o antigo nó da sub raiz também ser a raiz geral da árvore. Essa função posiciona
    os antigos filhos de nós rotacionados nas suas devidas novas posições. Atribuindo os ponteiros de filhos como também os de pais.
        Ao final da função as novas alturas são atribuídas, baseando-se na maior altura entre seus filhos mais 1.
        As rotações duplas são formadas pelas junções das rotações únicas, fazendo primeiro para um dos filhos e depois para a sub raiz.

2. Inserir:
	A função “inserir” retorna NULL caso seja impossível inserir o valor (o valor já foi inserido) e o nodo inserido caso seja possível. A função “inserir” serve como 
    função wrapper, onde a função “insere” é chamada, o diferencial dessa é que um de seus argumentos é um ponteiro para inteiros para que assim, seja possível 
    registrar se o nodo foi inserido ou não.
	2.1. Insere:
        Essa é uma função recursiva, onde a árvore é percorrida em forma de busca. Caso a busca acabe em um nodo null o novo nó é adicionado nessa posição e o ponteiro
        para inteiro irá sinalizar que a operação foi um sucesso, caso a busca acabe encontrando um nó em que a chave é a mesma do que deve ser adicionado, o ponteiro 
        para inteiros irá sinalizar que não foi possível realizar a operação.
        No retorno da recursão todos os nodos entre o adicionado e a raiz passarão pelo reajuste de altura e balanceamento.

3. excluir:
	A função “excluir” retorna 0 caso não seja possível excluir o nodo e 1 caso contrário. A função serve como função wrapper, onde a função “deleta” tem o diferencial,
    pois um de seus argumentos é um ponteiro para inteiro o qual tem como finalidade tornar se foi possível ou não excluir o nodo.
	3.1. deleta:
        Essa é uma função recursiva, onde a árvore é percorrida em forma de busca. Caso a busca acabe em um nodo null, será sinalizado através do ponteiro para inteiros
    que não foi possível executar a operação. Caso contrário a função “apagar” será chamada e o ponteiro para inteiros irá sinalizar que foi possível realizar a 
    operação. No retorno da recursão todos os nodos entre o excluído e a raiz irão passar pelo processo de ajuste de altura e balanceamento.

    3.2. apagar:
	    Essa função recebe como parâmetros o ponteiro que aponta para o ponteiro da raiz e o ponteiro para o nó que deve ser excluído. Para sua execução foi necessário
    a criação de duas outras funções:

	    3.2.1 Transplanta:
            Essa função tem como finalidade substituir um nó por um de seus filhos, reajustando tanto os ponteiros para pais como também os para filhos. 
        3.2.2 Antecessor:
	        Essa função tem como finalidade obter o nó mais à direita da sub arvore à esquerda do nó selecionado.

        Existem quatro casos na exclusão de um nodo de uma árvore avl. O primeiro caso é se o nó apagado não tiver um filho esquerdo, então o nó transplantado para a 
    posição do apagado deverá ser seu filho direito. O outro caso é a mesma coisa, mas se o nó apagado não possuir um filho direito assim, o nó transplantado para sua 
    posição será seu filho esquerdo. Esses casos são aplicados principalmente quando o nó a ser removido é uma folha.
        Os outros dois casos irão trabalhar com o antecessor do nó removido. O terceiro caso é quando o antecessor do nó é filho do nó apagado, então os dois são 
    transplantados e seus filhos são reajustados para suas devidas posições. O quarto e mais complexo caso, é quando o pai do antecessor não é o nó que será removido.
    O primeiro passo é realizar o transplante entre o antecessor e seu filho esquerdo, e depois disso já atribuir o filho esquerdo do nó a ser excluído como filho do 
    antecessor, ajustando as relações de filhos como também a de pais. O segundo passo é transplantar o antecessor para a posição do nó removido ajustando a sua 
    relação com o filho direito do nó agora removido.
        As alturas e o balanceamento são reajustadas desde a posição em que estava o nó antecessor até a raiz da árvore.

4. buscar:
	A função de busca acontece de forma recursiva, onde a árvore é percorrida baseada no valor das chaves, retorna NULL se o valor não for encontrado 
    (chegou em uma folha) ou um ponteiro para o valor do nó caso encontrado.

5. imprimirEmOrdem
	A árvore é primeiro percorrida até o extremo esquerdo (menor valor) e volta sendo pintada em ordem crescente printando primeiro o filho esquerdo e depois o direito.

6. ImprimirEmLargura
	Para a implementação dessa função foi necessário implementar uma fila, onde cada nodo da fila aponta para um nodo da árvore. A função funciona de forma simples, a 
raiz é adicionada na fila e enquanto a fila não estiver vazia, um valor será retirado desta e suas informações prestadas. Os filhos desse nó serão adicionados à fila.
	Para imprimir o nível e a usar as tabulações nos momentos corretos foi necessário sempre guardar as informações no nodo anteriormente printado, a tabulação e a
informação de nível são impressas apenas quando a chave do nodo atual é menor que a do anterior ou este mesmo nodo é filho direito do anterior.
	
